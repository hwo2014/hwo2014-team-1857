# require 'awesome_print'
require './track_strategy'

keimola = [
{:radius=>100, :angle=>45.0, :pieceIndex=>-6, :target=>1.0}, 
{:length=>100.0, :switch=>true, :pieceIndex=>-5, :target=>1.0}, 
{:length=>100.0, :pieceIndex=>-4, :target=>1.0}, 
{:length=>100.0, :pieceIndex=>-3, :target=>1.0}, 
{:length=>100.0, :pieceIndex=>-2, :target=>1.0}, 
{:length=>90.0, :pieceIndex=>-1, :target=>1.0},
{:length=>100.0, :pieceIndex=>0, :target=>1.0}, 
{:length=>100.0, :pieceIndex=>1, :target=>1.0}, 
{:length=>100.0, :pieceIndex=>2, :target=>0.5}, 
{:length=>100.0, :switch=>true, :pieceIndex=>3, :target=>0.0}, 
{:radius=>100, :angle=>45.0, :pieceIndex=>4, :target=>0.7}, 
{:radius=>100, :angle=>45.0, :pieceIndex=>5, :target=>0.7}, 
{:radius=>100, :angle=>45.0, :pieceIndex=>6, :target=>0.7}, 
{:radius=>100, :angle=>45.0, :pieceIndex=>7, :target=>1.0}, 
{:radius=>200, :angle=>22.5, :switch=>true, :pieceIndex=>8, :target=>1.0}, 
{:length=>100.0, :pieceIndex=>9, :target=>1.0}, 
{:length=>100.0, :pieceIndex=>10, :target=>1.0}, 
{:radius=>200, :angle=>-22.5, :pieceIndex=>11, :target=>1.0}, 
{:length=>100.0, :pieceIndex=>12, :target=>0.5}, 
{:length=>100.0, :switch=>true, :pieceIndex=>13, :target=>0.0}, 
{:radius=>100, :angle=>-45.0, :pieceIndex=>14, :target=>0.6}, 
{:radius=>100, :angle=>-45.0, :pieceIndex=>15, :target=>0.6}, 
{:radius=>100, :angle=>-45.0, :pieceIndex=>16, :target=>0.6}, 
{:radius=>100, :angle=>-45.0, :pieceIndex=>17, :target=>0.6}, 
{:length=>100.0, :switch=>true, :pieceIndex=>18, :target=>0.7}, 
{:radius=>100, :angle=>45.0, :pieceIndex=>19, :target=>0.7}, 
{:radius=>100, :angle=>45.0, :pieceIndex=>20, :target=>0.7}, 
{:radius=>100, :angle=>45.0, :pieceIndex=>21, :target=>0.7}, 
{:radius=>100, :angle=>45.0, :pieceIndex=>22, :target=>1.0}, 
{:radius=>200, :angle=>22.5, :pieceIndex=>23, :target=>1.0}, 
{:radius=>200, :angle=>-22.5, :pieceIndex=>24, :target=>1.0}, 
{:length=>100.0, :switch=>true, :pieceIndex=>25, :target=>0.0}, 
{:radius=>100, :angle=>45.0, :pieceIndex=>26, :target=>0.6}, 
{:radius=>100, :angle=>45.0, :pieceIndex=>27, :target=>0.6}, 
{:length=>62.0, :pieceIndex=>28, :target=>0.6}, 
{:radius=>100, :angle=>-45.0, :switch=>true, :pieceIndex=>29, :target=>0.6}, 
{:radius=>100, :angle=>-45.0, :pieceIndex=>30, :target=>0.6}, 
{:radius=>100, :angle=>45.0, :pieceIndex=>31, :target=>0.8}, 
{:radius=>100, :angle=>45.0, :pieceIndex=>32, :target=>0.8}, 
{:radius=>100, :angle=>45.0, :pieceIndex=>33, :target=>0.8}, 
{:radius=>100, :angle=>45.0, :pieceIndex=>34, :target=>1.0}, 
{:length=>100.0, :switch=>true, :pieceIndex=>35, :target=>1.0}, 
{:length=>100.0, :pieceIndex=>36, :target=>1.0}, 
{:length=>100.0, :pieceIndex=>37, :target=>1.0}, 
{:length=>100.0, :pieceIndex=>38, :target=>1.0}, 
{:length=>90.0, :pieceIndex=>39, :target=>1.0}
]

france = [
{"length"=>94.0, :pieceIndex =>-5},
{"length"=>94.0, :pieceIndex =>-4},
{"length"=>94.0, :pieceIndex =>-3},
{"length"=>94.0, :pieceIndex =>-2},
{"length"=>91.0, :pieceIndex =>-1},
{"length"=>102.0, :pieceIndex =>0},
{"length"=>94.0, :pieceIndex =>1},
{"radius"=>200, "angle"=>22.5, "switch"=>true, :pieceIndex =>2},
{"radius"=>200, "angle"=>22.5, :pieceIndex =>3},
{"radius"=>200, "angle"=>22.5, :pieceIndex =>4},
{"radius"=>200, "angle"=>22.5, :pieceIndex =>5},
{"radius"=>100, "angle"=>45.0, :pieceIndex =>6},
{"radius"=>100, "angle"=>45.0, "switch"=>true, :pieceIndex =>7},
{"length"=>53.0, :pieceIndex =>8},
{"radius"=>200, "angle"=>-22.5, :pieceIndex =>9},
{"radius"=>50, "angle"=>-45.0, :pieceIndex =>10},
{"radius"=>100, "angle"=>45.0, :pieceIndex =>11},
{"radius"=>100, "angle"=>45.0, :pieceIndex =>12},
{"radius"=>100, "angle"=>45.0, :pieceIndex =>13},
{"radius"=>50, "angle"=>-45.0, :pieceIndex =>14},
{"radius"=>50, "angle"=>-45.0, :pieceIndex =>15},
{"radius"=>50, "angle"=>-45.0, :pieceIndex =>16},
{"radius"=>200, "angle"=>-22.5, "switch"=>true, :pieceIndex =>17},
{"length"=>94.0, :pieceIndex =>18},
{"length"=>94.0, :pieceIndex =>19},
{"radius"=>50, "angle"=>45.0, :pieceIndex =>20},
{"radius"=>50, "angle"=>45.0, :pieceIndex =>21},
{"radius"=>50, "angle"=>45.0, :pieceIndex =>22},
{"radius"=>50, "angle"=>45.0, :pieceIndex =>23},
{"length"=>99.0, "switch"=>true, :pieceIndex =>24},
{"length"=>99.0, :pieceIndex =>25},
{"radius"=>100, "angle"=>-45.0, :pieceIndex =>26},
{"radius"=>100, "angle"=>-45.0, :pieceIndex =>27},
{"radius"=>100, "angle"=>-45.0, :pieceIndex =>28},
{"radius"=>100, "angle"=>45.0, :pieceIndex =>29},
{"radius"=>200, "angle"=>22.5, "switch"=>true, :pieceIndex =>30},
{"radius"=>200, "angle"=>22.5, :pieceIndex =>31},
{"length"=>53.0, :pieceIndex =>32},
{"radius"=>200, "angle"=>22.5, :pieceIndex =>33},
{"radius"=>200, "angle"=>22.5, :pieceIndex =>34},
{"radius"=>100, "angle"=>45.0, :pieceIndex =>35},
{"radius"=>200, "angle"=>22.5, :pieceIndex =>36},
{"radius"=>200, "angle"=>22.5, :pieceIndex =>37},
{"length"=>94.0, "switch"=>true, :pieceIndex =>38},
{"length"=>94.0, :pieceIndex =>39},
{"length"=>94.0, :pieceIndex =>40},
{"length"=>94.0, :pieceIndex =>41},
{"length"=>94.0, :pieceIndex =>42},
{"length"=>91.0, :pieceIndex =>43}]

france = TrackStrategy.strategize(france)

france.each_with_index do |piece, index|
    throttle = piece["throttle"]
    if piece[:pieceIndex] >= 0
        radius = piece["radius"] || 0
        angle = piece["angle"] || 0
        length = piece["length"] || 0

        if piece[:pieceIndex] % 5 == 0
            printf "%2s %8s %8s %8s %8s %8s %8s %8s\n","pc","radius","angle", "length", "target", "throttle", "speed_X", "turn"
            printf "=======================================================================================================\n"
        end

        throttle = throttle.round(2)

        printf "%2s %8s %8s %8s %8s %8s %8s %8s\n",piece[:pieceIndex],radius,angle, length, piece[:target], throttle, piece["speed_X"], piece["turn_factor"].round(2)
        printf "---------------------------------------------------------------------------------------------------------------------\n"
    end


end
