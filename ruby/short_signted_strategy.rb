class ShortSigntedStrategy
  def self.strategize(track)
    track.each_with_index do |pc, index|
      throttle = 1.0

      look_back = 4
      look_ahead = 1
      look_ahead_skip = 1
      pucker_threshold = 3.8
      pucker_smooth = 2.4
      speed_look_back = 1

      speed_4, speed_3, speed_2, speed_1, speed_X = 0, 0, 0, 0, 0
      track[(index-4)...index].each do |pc|
          speed_4 = speed_4 + (pc["throttle"] || 1)
      end

      track[index-3...index].each do |pc|
          speed_3 = speed_3 + (pc["throttle"] || 1)
      end

      track[index-2...index].each do |pc|
          speed_2 = speed_2 + (pc["throttle"] || 1)
      end

      track[index-1...index].each do |pc|
          speed_1 = speed_1 + (pc["throttle"] || 1)
      end

      speed_X = (speed_4 * 0.06) + (speed_3 * 0.06) + (speed_2 * 0.08) + (speed_1 * 0.80)

      #
      # some voodoo concerning the upcoming pieces
      #
      turn_factor = 0
      track[index+look_ahead_skip...index+look_ahead_skip+look_ahead].each_with_index do |pc, idx|
        radius = pc["radius"] || 0
        radius_factor = 1

        if radius > 0 && radius < 100
          radius_factor = (126.0 / radius)
        end

        tmp = (((pc["angle"] || 0) * (radius_factor)).abs / 90) / (2 + idx)
        turn_factor = turn_factor + tmp
        pc["radius_factor"] = radius_factor
      end

      if Math.sin(speed_X).round(2) > 0.86 && Math.tan(turn_factor) > 0.48
          throttle = 0
      else
          throttle = 1 - ((Math.sin(speed_X).round(2) * 1.1) * Math.tan(turn_factor))
      end

      if throttle < 0
        throttle = 0.01
      end

      pc["throttle"] = throttle.round(2)
      pc["speed_X"] = Math.sin(speed_X).round(2)
      pc["turn_factor"] = Math.tan(turn_factor).round(2)
    end

    track
  end

  def self.crashed_at(piece_number, track)

    radius = track[piece_number]["radius"]
    dampen = (!radius.nil? && radius < 100) ? 0.3 : 0.2 

    (piece_number-3..piece_number-1).each do |index|
      new_throttle = track[index]["throttle"] - dampen
      if new_throttle < 0
        new_throttle = 0.01
      end
      puts "Resetting #{track[index]["throttle"]} to #{new_throttle}"
      track[index]["throttle"] = new_throttle
    end
    track
  end
end