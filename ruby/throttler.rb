require 'json'
require './track_strategy'
require './slow_strategy'
require './long_throttle_strategy'
require './straightaway_strategy'
require './short_signted_strategy'
require './conservative_strategy'
# require 'awesome_print'

class Throttler
  attr_accessor :track_name, :game_id, :track, :car_color

  def initialize(tcp, track_strategy_class)
    @tcp = tcp
    @angles = [0.0]
    @csv_rows = []
    @turboMsg = nil
    @turboTicks = 0
    @turboBoost = 0
    @track=[]
    @track_strategy = track_strategy_class
  end

  def game_start()
    1.0
  end

  def throttle(carPosition)
    pieceIndex = carPosition['piecePosition']['pieceIndex']    
    radius = @track[pieceIndex]['radius'] || 0
    pieceAngle = @track[pieceIndex]['angle'] || 0

    angle = carPosition['angle'].abs.round(3)
    angle_delta_raw = (angle - @angles.last).round(3)
    angle_delta = angle_delta_raw.abs
    
    puts "angle_delta #{angle} #{angle_delta}"
    
    @angles << angle
    throttle = @track[pieceIndex]["throttle"]    
    throttle
  end

  def turbo_engaged
    @turboTicks > 0
  end

  def track=(track_pieces)
    @track = @track_strategy.strategize(track_pieces) if @track.empty?
    # ap @track 
    @track
  end

  def we_care_about_this(color) 
    @car_color == color
  end

  def crashed_at(crashMsg, piece)
    color = crashMsg["color"]
    crashed_at = piece["piecePosition"]["pieceIndex"]
    puts "#{color} Crashed at: #{crashed_at}"

    if we_care_about_this(color)
      @track = @track_strategy.crashed_at(crashed_at, track)
    else
      puts "But we don't care because our color is #{@car_color}"
    end

    track[crashed_at]["throttle"]
  end

  def turbo(turboMsg, piece)
    @turboMsg = turboMsg
    pieceIndex = piece["piecePosition"]["pieceIndex"]
    track[pieceIndex]["throttle"]
  end

  def throttle_message(throttle)
    make_msg("throttle", throttle)
  end

  def make_msg(msgType, data)
    msg = {:msgType => msgType, :data => data}
    #puts "<<<< #{msg}"
    JSON.generate(msg)
  end
end