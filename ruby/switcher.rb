class Switcher

  # An array of track pieces hashes
  @@track = []

  # An array of switches. A switch at index 5 indicates track piece five should switch Right
  # Values will either be Switcher::LEFT, Switcher::RIGHT or nil
  @@switches = []

  RIGHT = "Right"
  LEFT  = "Left"

  def self.switch?(track_piece_num)
    # nil
    @@switches[track_piece_num]
  end  

  def self.track=(json_track_pieces)
    @@track = json_track_pieces
    parse_track
  end

  def self.track
    @@track
  end

  #
  # Strategy is to parse the track. When right turn, go back to the last switch piece and record RIGHT
  # if a left turn, go back to the last switch piece and record LEFT
  #
  def self.parse_track
    last_switch = 0
    @@track.each_with_index do |track_piece, index|
      switch = track_piece['switch']
      last_switch = index unless switch.nil?

      angle = track_piece['angle']
      unless angle.nil?
        if angle > 0         #right turn  
          @@switches[index] = RIGHT
        else                 #left turn
          @@switches[index] = LEFT
        end
      end
    end
    # puts @@switches.inspect
  end
end