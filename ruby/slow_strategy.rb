class SlowStrategy
  def self.strategize(track)
    track.each_with_index do |pc, index|
      radius = pc["radius"] || 0
      if radius == 0
        pc["throttle"] = 0.45
      elsif radius < 100
        pc["throttle"] = 0.2
      else
        pc["throttle"] = 0.4
      end
    end
    track
  end

  def self.crashed_at(piece_number, track)
    (piece_number-3..piece_number-1).each do |index|
      new_throttle = track[index]["throttle"] - 0.1
      if new_throttle < 0
        new_throttle = 0.01
      end
      puts "Resetting #{track[index]["throttle"]} to #{new_throttle}"
      track[index]["throttle"] = new_throttle
    end
    track
  end
end