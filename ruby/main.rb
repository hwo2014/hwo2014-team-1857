require './switcher'
require './throttler'
require 'json'
require 'socket'

# require 'awesome_print'

puts "ARGV[0] #{ARGV[0]}"
puts "ARGV[1] #{ARGV[1]}"
puts "ARGV[2] #{ARGV[2]}"
puts "ARGV[3] #{ARGV[3]}"
puts "ARGV[4] #{ARGV[4]}"
puts "ARGV[5] #{ARGV[5]}"
puts "ARGV[6] #{ARGV[6]}"

server_host = ARGV[0]
server_port = ARGV[1]
team_name   = ARGV[2]

# CI server sends bot_key in command line args for some reason
bot_key     = ARGV[3]
bot_key     = "4sN6UP4el+goug"

join            = ARGV[4].nil?
message_type    = ARGV[4] || "createRace"
track_strategy  = ARGV[5] || "TrackStrategy"
bot_name        = ARGV[6] || "mark_martin"
track_name      = ARGV[7] || "keimola"

puts "#{bot_name} connecting to #{server_host}:#{server_port} using #{track_strategy}"

class MarkMartin
  def initialize(server_host, server_port, bot_name, bot_key, track_name, track_strategy, message_type, join)
    
    puts "server_host #{server_host}"
    puts "server_port #{server_port}"
    puts "bot_name #{bot_name}"
    puts "bot_key #{bot_key}"
    puts "track_name #{track_name}"
    puts "track_strategy #{track_strategy}"
    puts "message_type #{message_type}"
    puts "join #{join}"

    tcp = TCPSocket.open(server_host, server_port)
    @track_name = track_name
    @track_strategy = track_strategy
    @throttler = Throttler.new(tcp, Kernel.const_get(track_strategy))
    @throttler.track_name = track_name
    @lastPosition = nil
    @last_switch = nil
    play(bot_name, bot_key, tcp, track_name, message_type, join)
  end

  private

  def car_position(msgData, bot_name)
    msgData.each_with_index do |data|
      return data if (data["id"]["name"] == bot_name)
    end
  end

  def play(bot_name, bot_key, tcp, track_name, message_type, join)
    tcp.puts create_message(bot_name, bot_key, track_name, message_type, join)
    react_to_messages_from_server(tcp, bot_name)
  end

  def react_to_messages_from_server(tcp, bot_name)
    while json = tcp.gets
      message = JSON.parse(json)
      msgType = message['msgType']
      msgData = message['data']
      # puts ">>>> #{message}"
      case msgType
        when 'gameInit'
          #ap msgData['race']['track']['pieces']

          puts ">>>> #{message}"
          Switcher.track = msgData['race']['track']['pieces']
          @throttler.track = msgData['race']['track']['pieces']
          tcp.puts ping_message
        when 'carPositions'
          @lastPosition = car_position(msgData, bot_name)
          piece_index = car_position(msgData, bot_name)['piecePosition']['pieceIndex']
          switch_to = Switcher.switch?(piece_index)
          
          if switch_to.nil? || piece_index == @last_switch || @track_strategy != "TrackStrategy"
            tcp.puts throttle_message(@throttler.throttle(car_position(msgData, bot_name)))
          else
            @last_switch = piece_index
            tcp.puts switch_message(switch_to)
          end
        when 'gameStart'
          puts "gameStart"
          tcp.puts throttle_message(@throttler.game_start)
        when 'crash'
          puts 'Such crash!'
          tcp.puts throttle_message(@throttler.crashed_at(msgData, @lastPosition))
        when 'turboAvailable'
          tcp.puts throttle_message(@throttler.turbo(msgData, @lastPosition))
        when 'yourCar'
          puts ">>>> #{message}"
          @game_id = message['gameId']
          @car_color = msgData['color']
          puts "game_id [#{@game_id}]"
          @throttler.game_id = @game_id
          @throttler.car_color = @car_color
          tcp.puts ping_message
        else
          case msgType
            when 'error'
              puts "ERROR: #{msgData}"
          end
          puts "Got #{msgType}"
          puts message
          tcp.puts ping_message
      end
    end
  end

  def switch_message(lane)
    JSON.generate({:msgType => "switchLane", :data => lane})
  end

  def turbo_message
    JSON.generate({:msgType => "turbo", :data => "much fast"})
  end

  def join_message(bot_name, bot_key)
    make_msg("join", {:name => bot_name, :key => bot_key})
  end

  def throttle_message(throttle)
    make_msg("throttle", throttle)
  end

  def ping_message
    make_msg("ping", {})
  end

  def create_message(bot_name, bot_key, track_name, message_type, join)
    if join
      make_msg("join", {:name => bot_name, :key => bot_key})
    else
      puts message_type
      make_msg("#{message_type}", {:botId => {:name => bot_name, :key => bot_key}, :trackName => track_name, :password => 'crystal_palace', :carCount => 2})
    end
  end

  def make_msg(msgType, data)
    msg = {:msgType => msgType, :data => data}
    JSON.generate(msg)
  end
end

MarkMartin.new(server_host, server_port, bot_name, bot_key, track_name, track_strategy, message_type, join)
